//
//  main.cpp
//  Jason_HW1
//
//  Created by Anqing Liu on 9/15/15.
//  Copyright (c) 2015 Anqing Liu. All rights reserved.
//

#include <opencv2/highgui/highgui.hpp>
#include "opencv2/imgproc.hpp"
#include "opencv2/core.hpp"
#include "opencv2/videoio.hpp"
#include "opencv2/objdetect/objdetect.hpp"
#include <iostream>
#include <fstream>
#include <stdio.h>

using namespace std;
using namespace cv;


int main(int argc, const char** argv)
{
    ofstream outputFile;
    outputFile.open("/Users/Jason/Documents/ND2015_Fall/Biometrics/Jason_HW1/Self collected images/OwnResults.txt");
    
    std::ifstream infile("/Users/Jason/Documents/ND2015_Fall/Biometrics/Jason_HW1/Self collected images/selfPics.txt");
    string str;
    int n =0;
    
    while (std::getline(infile,str)) {
        
    
     Mat imag = imread("/Users/Jason/Documents/ND2015_Fall/Biometrics/Jason_HW1/Self collected images/"+str+".jpg");
    //create the cascade classifier object used for the face detection
    CascadeClassifier face_cascade;
    //use the haarcascade_frontalface_alt.xml library
    face_cascade.load("/Users/Jason/Documents/ND2015_Fall/Biometrics/opencv-3.0.0/data/haarcascades/haarcascade_frontalface_alt.xml");
    
    //setup video capture device and link it to the first capture device
    //VideoCapture captureDevice;
    //captureDevice.open(0);
    
    //setup image files used in the capture process
    
    Mat grayscaleFrame;
        n++;
    
    //create a window to present the results
    namedWindow("outputCapture"+to_string(n), WINDOW_AUTOSIZE);

        //capture a new image frame
        //captureDevice>>captureFrame;
        
        //convert captured image to gray scale and equalize
        cvtColor(imag, grayscaleFrame, CV_BGR2GRAY);
        equalizeHist(grayscaleFrame, grayscaleFrame);
        
        //create a vector array to store the face found
        vector<Rect> faces;
        vector<int> rejectLevels;
        vector<double> levelWeights;
        
        //find faces and store them in the vector array
     face_cascade.detectMultiScale(grayscaleFrame, faces,rejectLevels,levelWeights, 1.1, 3,0,  Size(),Size(),true);
     
        //draw a rectangle for all found faces in the vector array on the original image
        for(int i = 0; i < faces.size(); i++)
        {
            Point pt1(faces[i].x + faces[i].width, faces[i].y + faces[i].height);
            Point pt2(faces[i].x, faces[i].y);
            
            rectangle(imag, pt1, pt2, cvScalar(0, 255, 0, 0), 1, 8, 0);
            //1000*last_stage + stage_sum_for_last_stage;
            //cout << (faces[i].size());
            putText(imag, std::to_string(levelWeights[i]-100), Point(faces[i].x,faces[i].y),1,1,Scalar(0,0,255));
        }
        // print the detection output
        outputFile <<  str  << endl;
        outputFile <<  to_string(faces.size()) << endl;
        for(int i = 0; i < faces.size(); i++){
            
            outputFile << to_string(faces[i].x)+" " + to_string(faces[i].y) +" " +
            to_string(faces[i].width) +" " + to_string(faces[i].height) + + " " +to_string(levelWeights[i]) << endl;
        }
        
        //print the output
        if (n%1==0){
            cout << "<" + str + ">" << endl;
            cout << "<" + to_string(faces.size()) + ">"<< endl;
           for(int i = 0; i < faces.size(); i++){
                cout << "<" + to_string(faces[i].x)+" " + to_string(faces[i].y) +" " +
                to_string(faces[i].width) +" " + to_string(faces[i].height) + + " " +to_string(levelWeights[i])+">" << endl;
            }
        namedWindow("outputCapture"+to_string(n), WINDOW_AUTOSIZE);
        imshow("outputCapture"+to_string(n), imag);
        //waitKey(0);
            
            
        }
    
    }
    outputFile.close();
    return 0;
}